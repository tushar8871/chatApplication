sudo su -
sudo apt-get update
sudo apt-get install python3-pip python3-dev nginx git -y
sudo apt-get update
#cd /chatApplication
sudo pip3 install virtualenv
sudo virtualenv venv
#source venv/bin/activate
sudo venv/bin/pip3 install -r requirements.txt
sudo venv/bin/pip3 install django-bcrypt django-extensions
sudo venv/bin/pip3 install gunicorn
cd fundoo/
sudo /chatApplication/venv/bin/python3 manage.py collectstatic --noinput
sudo cp /chatApplication/Dependency_Scripts/gunicorn.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl start gunicorn
sudo systemctl enable gunicorn
sudo unlink /etc/nginx/sites-enabled/*
sudo cp /chatApplication/Dependency_Scripts/fundoo /etc/nginx/sites-available/
sudo ln -s /etc/nginx/sites-available/fundoo /etc/nginx/sites-enabled
sudo systemctl restart nginx
